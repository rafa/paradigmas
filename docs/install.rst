**Instalaci�n de Haskell**
==========================

Para poder programar en Haskell necesitamos descargar e instalar el conjunto de librer�as, 
int�rprete y compilador. La distribuci�n m�s activa es GHC (Glasgow Haskell Compiler). 
ghc es el compilador optimizado para generar r�pido c�digo nativo; ghci es el int�rprete y depurador interactivo. 

**Microsoft Windows**
:::::::::::::::::::::

Pasos:
......

1. Ir al sitio oficial del lenguaje de programaci�n Haskell_. 
2. Buscar y hacer click en la secci�n Downloads_ (Descargas).

.. image:: images/haskell.png
   :scale: 50 %                          
   :target: http://www.haskell.org/downloads
   :alt: Sitio oficial del proyecto.

   
3. Seleccionar Windows.

.. image:: images/haskell_download.png                         
   :target: http://www.haskell.org/downloads/windows

   
4. Seleccionar y hacer click para descargar el ejecutable MinGHC, dependiendo de la arquitectura de su computadora (32 o 64 bits).

.. image:: images/haskell_arch.png                         
   
   
5. Guarde el instalador en su computadora.

.. image:: images/haskell_save_file.png                         


6. Una vez finalizada la descarga, ejecutar el instalador.

7. Seleccionar el directorio a d�nde se va instalar y darle aceptar. (En netbooks con escasa memoria RAM, se puede demorar varios minutos.)

8. Buscar la carpeta a donde se instal� y ejecutar ghci.exe para abrir el int�rprete de Haskell.

**GNU/Linux**
:::::::::::::

La instalaci�n de Haskell la vamos a realizar en la distribuci�n Debian, por lo cual en distribuciones derivadas como Ubuntu o Linux Mint el procedimiento de instalaci�n es similar y deber�a funcionar sin inconvenientes.

Pasos:
......

1. Configurar las fuentes del gestor de paquetes (APT) editando el archivo sources.list:

.. code-block:: console

   # vi /etc/apt/sources.list

2. Agregar alguno de los repositorios de Argentina:

.. code-block:: console

   deb http://mirrors.dcarsat.com.ar/debian/ stable main contrib non-free
   deb-src http://mirrors.dcarsat.com.ar/debian/ stable main contrib non-free

2. Verificar que se tiene conectividad con el repositorio de Debian, actualizando el �ndice de paquetes: 

.. code-block:: console

   # apt-get update

3. Buscar los paquetes de ghc (Glasgow Haskell Compiler): 

.. code-block:: console

   # apt-cache search glasgow
   bnfc - Compiler front-end generator based on Labelled BNF
   ghc - The Glasgow Haskell Compilation system
   ghc-doc - Documentation for the Glasgow Haskell Compilation system
   ghc-dynamic - Dynamic libraries for the Glasgow Haskell Compilation system
   ghc-prof - Profiling libraries for the Glasgow Haskell Compilation system

   
4. Instalar las librer�as y respectivas dependencias:

.. code-block:: console

   # apt-get install ghc
   Leyendo lista de paquetes... Hecho
   Creando �rbol de dependencias       
   Leyendo la informaci�n de estado... Hecho
   Se instalar�n los siguientes paquetes extras:
       binutils cpp cpp-4.9 gcc gcc-4.9 libasan0 libasan1 libatomic1 libbsd-dev libcilkrts5
       libcloog-isl4 libffi-dev libgcc-4.8-dev libgcc-4.9-dev libgmp-dev libgmpxx4ldbl libgomp1
       libisl10 libitm1 liblsan0 libmpc3 libmpfr4 libquadmath0 libstdc++-4.8-dev libtsan0 libubsan0
   Paquetes sugeridos:
       binutils-doc cpp-doc gcc-4.9-locales gcc-multilib make autoconf automake libtool flex bison
       gdb gcc-doc gcc-4.9-multilib gcc-4.9-doc libgcc1-dbg libgomp1-dbg libitm1-dbg libatomic1-dbg
       libasan1-dbg liblsan0-dbg libtsan0-dbg libubsan0-dbg libcilkrts5-dbg libquadmath0-dbg
       ghc-prof ghc-doc haskell-doc llvm libgmp10-doc libmpfr-dev libstdc++-4.8-doc
   Se instalar�n los siguientes paquetes NUEVOS:
       binutils cpp cpp-4.9 gcc gcc-4.9 ghc libasan0 libasan1 libatomic1 libbsd-dev libcilkrts5
       libcloog-isl4 libffi-dev libgcc-4.8-dev libgcc-4.9-dev libgmp-dev libgmpxx4ldbl libgomp1
       libisl10 libitm1 liblsan0 libmpc3 libmpfr4 libquadmath0 libstdc++-4.8-dev libtsan0 libubsan0
   0 actualizados, 27 nuevos se instalar�n, 0 para eliminar y 23 no actualizados.
   Se necesita descargar 47,1 MB de archivos.
   Se utilizar�n 384 MB de espacio de disco adicional despu�s de esta operaci�n.
   �Desea continuar? [S/n]
   
5. Para verificar la instalaci�n exitosa, abra una consola y ejecute el int�rprete de Haskell:

.. code-block:: console

   $ ghci
   Prelude>

.. _Haskell: http://www.haskell.org
.. _Downloads: http://www.haskell.org/downloads